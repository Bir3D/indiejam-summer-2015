﻿using UnityEngine;
using System.Collections;

public abstract class SingleUseInteractable : InteractableObject {
	protected bool isUsed;
}
