﻿using UnityEngine;
using System.Collections;

public abstract class LinkedInteractable : InteractableObject {
	public AffectableObject affectablePartner;
}
