using UnityEngine;
using System.Collections;

public class Pressureplate : LinkedToggleInteractable {

	Vector3 posStart;
	Vector3 posEnd;
	bool buttonActivate = true;
	public float timeBeforeGoingUp;
	public AudioClip[] arrayAudioClips = new AudioClip[2];
	AudioSource sound;




	// Use this for initialization
	void Start () {
		sound = gameObject.GetComponent<AudioSource> ();
		posStart = transform.position;
		posEnd = transform.position - Vector3.up * 0.05f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionEnter (Collision col) {
		if (col.collider.tag == "Player") {
			if(buttonActivate == true){
				affectablePartner.Affect();
				transform.position = posEnd;
				sound.clip = arrayAudioClips[0];
				sound.Play();
				StartCoroutine(Delay());

			}
		}
		
	}

	void OnCollisionExit (Collision col) {
		if (col.collider.tag == "Player") {
			if(buttonActivate == true){
				affectablePartner.Affect();
				transform.position = posStart;
				sound.clip = arrayAudioClips[1];
				sound.Play();
				StartCoroutine(Delay());
			}
		}
	}

	IEnumerator Delay (){
		buttonActivate = false;
		yield return new WaitForSeconds(timeBeforeGoingUp);
		buttonActivate = true;
	}
}
