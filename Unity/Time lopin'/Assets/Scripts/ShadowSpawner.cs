﻿using UnityEngine;
using System.Collections;

public class ShadowSpawner : MonoBehaviour {
	public Shadow shadowPrefab;
	
	// Use this for initialization
	void Start () {
		foreach(Recording rec in Handler.handlerInstance.AllShadowRecordings()) {
			SpawnShadow(rec);
		}
	}
	
	private void SpawnShadow (Recording rec) {
		Shadow newShadow = Instantiate(shadowPrefab) as Shadow;
		newShadow.transform.position = transform.position;
		newShadow.transform.rotation = transform.rotation;
		newShadow.myRecording = new Recording(rec);
	}
}
