﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Recording {
	public List<MovementState> recordedMovementStates;
	public List<float> recordedTimeStamps;
	
	public Recording () {
		recordedMovementStates = new List<MovementState>();
		recordedTimeStamps = new List<float>();
	}
	
	public Recording (Recording recordingToCopy) {
		recordedMovementStates = new List<MovementState>(recordingToCopy.recordedMovementStates);
		recordedTimeStamps = new List<float>(recordingToCopy.recordedTimeStamps);
	}
	
	public void AddState(MovementState stateToAdd, float timeStamp) {
		recordedMovementStates.Add(stateToAdd);
		recordedTimeStamps.Add(timeStamp);
	}
	
	public MovementState ReadNextState () {
		return recordedMovementStates[0];
	}
	
	public float ReadNextTimeStamp () {
		return recordedTimeStamps[0];
	}
	
	public void RemoveNextState () {
		recordedMovementStates.RemoveAt(0);
		recordedTimeStamps.RemoveAt(0);
	}
}
