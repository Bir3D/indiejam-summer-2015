﻿using UnityEngine;
using System.Collections;

public class OneWayPiston : MonoBehaviour {
	public float speed;
	public float distanceToMove;
	public float timeWaiting;
	
	void Start () {
		StartCoroutine(WaitAndMove());
	}
	
	IEnumerator WaitAndMove() {
		yield return new WaitForSeconds(timeWaiting);
		Vector3 startPosition = transform.position;
		while (Vector3.Distance(startPosition, transform.position) < distanceToMove) {
			transform.Translate(Vector3.up * speed * Time.deltaTime);
			yield return null;
		}
		if(speed > 0) {
			transform.position = startPosition + Vector3.up * distanceToMove;	
		}
		else {
			transform.position = startPosition - Vector3.up * distanceToMove;	
		}
	}
}
