﻿using UnityEngine;
using System.Collections;

//This class is for all objects that can be affected by a LinkedInteractableObject
public abstract class AffectableObject : MonoBehaviour {
	public abstract void Affect();
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
