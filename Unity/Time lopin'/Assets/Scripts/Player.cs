﻿using UnityEngine;
using System.Collections;

public class Player : MovingCharacter {
	public override MovementState currentMovementState {
		get {
			return base.currentMovementState;
		}
		set {
			if (value != currentMovementState) {
				Handler.handlerInstance.AddState(value);
			}
			base.currentMovementState = value;
		}
	}
	
	protected override void Start ()
	{
		base.Start ();
		Handler.handlerInstance.StartNewShadowRecording();
	}
	
	protected override void Update ()
	{
		base.Update ();
		if (Input.GetKeyDown(KeyCode.Space)) {
			currentMovementState = MovementState.idle;
			
			Application.LoadLevel(Application.loadedLevel);
		}
		else {
			if (Input.GetKeyDown(KeyCode.W)){
				currentMovementState = MovementState.jumping;
			}
			if (currentMovementState != MovementState.jumping) {
				if(Input.GetAxis("Horizontal") > 0) {
					currentMovementState = MovementState.runningRight;
				}
				else if (Input.GetAxis("Horizontal") < 0) {
					currentMovementState = MovementState.runningLeft;
				}
				else {
					currentMovementState = MovementState.idle;
				}
			}
		}
		
	}
	
	protected override void KillCharacter ()
	{
		currentMovementState = MovementState.idle;
		Application.LoadLevel(Application.loadedLevel);
	}
	
	protected override void JustJumped ()
	{
		if(Input.GetAxis("Horizontal") > 0) {
			currentMovementState = MovementState.runningRight;
		}
		else if (Input.GetAxis("Horizontal") < 0) {
			currentMovementState = MovementState.runningLeft;
		}
		else {
			currentMovementState = MovementState.idle;
		}
	}	
}
