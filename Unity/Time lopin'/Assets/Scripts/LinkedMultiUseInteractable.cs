﻿using UnityEngine;
using System.Collections;

public abstract class LinkedMultiUseInteractable : LinkedInteractable {
	protected override void InteractWith ()
	{
		affectablePartner.Affect();
	}
}
