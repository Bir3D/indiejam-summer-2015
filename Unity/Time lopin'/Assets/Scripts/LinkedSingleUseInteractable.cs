﻿using UnityEngine;
using System.Collections;

public abstract class LinkedSingleUseInteractable : LinkedInteractable {
	protected bool isUsed;
	
	protected override void InteractWith ()
	{
		if (!isUsed) {
			affectablePartner.Affect();
		}
	}
}

