﻿using UnityEngine;
using System.Collections;

public enum MovementState {jumping, runningLeft, runningRight, idle, interacting};

public abstract class MovingCharacter : MonoBehaviour {
	public Key currentKey;
	public Animator ani;
	public bool turningLeft;
	protected int groundTouched;
	private MovementState _currentMovementState = MovementState.idle;
<<<<<<< HEAD
	public float jumpForce;
	private Rigidbody m_Rigidbody;
	private BoxCollider m_BoxCollider;
	public float movementSpeed;
	public float movementSpeedInAir;
	public bool touchingTop;
	public bool touchingGround;
	
=======
>>>>>>> 0b97b8cb7dd273f925ceac3cba97697e3a2a69a0
	public virtual MovementState currentMovementState {
		get {
			return _currentMovementState;
		}
		set {
			_currentMovementState = value;
		}
	}
	public float jumpForce;
	private Rigidbody m_Rigidbody;
	public float movementSpeed;
	public float movementSpeedInAir;
	
	// Use this for initialization
	protected virtual void Start () {
	
	}
	
	// Update is called once per frame
	protected virtual void Update () {
<<<<<<< HEAD
		if (!(touchingTop && touchingGround)) {
			if (currentMovementState == MovementState.jumping) {
				ani.Play ("jumping");
			} else if (currentMovementState == MovementState.runningRight && groundTouched.Count > 0) {
				ani.Play ("running");
			} else if (currentMovementState == MovementState.runningLeft && groundTouched.Count > 0) {
				ani.Play ("running");
			} else if (currentMovementState == MovementState.idle && groundTouched.Count > 0) {
				ani.Play ("idle");
			} else if (currentMovementState == MovementState.interacting) {
				ani.Play ("pressButton");
			}
		}
		else {
			ani.Play("squashedDeath");
		}
=======
		
>>>>>>> 0b97b8cb7dd273f925ceac3cba97697e3a2a69a0
	}


	protected virtual void Awake()
	{
		// Setting up references.
		m_Rigidbody = GetComponent<Rigidbody>();
	}

	protected virtual void FixedUpdate () {

		switch (currentMovementState) {
			case MovementState.idle:
					m_Rigidbody.velocity = m_Rigidbody.velocity.y * Vector3.up;
				break;
			case MovementState.jumping:
				if (groundTouched > 0) {
					GetComponent<Rigidbody>().AddForce(0, jumpForce, 0, ForceMode.VelocityChange); 
				}
				JustJumped();
				break;
			case MovementState.runningLeft:
				if(groundTouched == 0) {
					if (m_Rigidbody.velocity.x > -movementSpeedInAir) {
						m_Rigidbody.velocity = -movementSpeedInAir * Vector3.right + m_Rigidbody.velocity.y * Vector3.up;
					}
				}
				else {
					m_Rigidbody.velocity = -movementSpeed * Vector3.right + m_Rigidbody.velocity.y * Vector3.up;
				}
				break;
			case MovementState.runningRight:
				if(groundTouched == 0) {
					if (m_Rigidbody.velocity.x < movementSpeedInAir) {
						m_Rigidbody.velocity = movementSpeedInAir * Vector3.right + m_Rigidbody.velocity.y * Vector3.up;
					}
				}
				else {
					m_Rigidbody.velocity = movementSpeed * Vector3.right + m_Rigidbody.velocity.y * Vector3.up;
				}
				break;
			case MovementState.interacting:
				break;
			default:
				throw new System.NotImplementedException();
		}
	}
	
	protected virtual void OnCollisionEnter (Collision col) {
		if (col.collider.tag == "Ground") {
<<<<<<< HEAD
			touchingGround = true;
			if(IsAboveObject((BoxCollider)col.collider)) {
				groundTouched.Add(col.collider);
			}
=======
			groundTouched++;
>>>>>>> 0b97b8cb7dd273f925ceac3cba97697e3a2a69a0
		}
		
		if (col.collider.tag == "Destroyer") {
			
			KillCharacter();
		}

		if (col.collider.tag == "topPiston") {
			touchingTop = true;
		}
	}
	
	protected virtual void KillCharacter () {
		Destroy(gameObject);
	}
	
	protected virtual void OnCollisionExit (Collision col) {
		if (col.collider.tag == "Ground") {
			groundTouched--;
		}
	}
	
	protected abstract void JustJumped();
}
