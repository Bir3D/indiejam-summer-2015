﻿using UnityEngine;
using System.Collections;

public class Piston : MonoBehaviour {
	public float speed;
	protected bool movingDown;
	public float timeMoving;
	
	void Start () {
		
	}
	
	
	public Piston ()
	{
		movingDown = true;
	}
	
	void FixedUpdate () {
		StartCoroutine(Delay());
	}
	
	IEnumerator Delay (){
		if (movingDown) { 
			transform.Translate(-speed,0,0);
			yield return new WaitForSeconds(timeMoving);
			movingDown = false;
		}
		else {
			transform.Translate(speed,0,0);
			yield return new WaitForSeconds(timeMoving);
			movingDown = true;
		}
	}
}
