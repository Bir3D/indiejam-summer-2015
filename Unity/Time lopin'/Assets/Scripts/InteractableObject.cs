﻿using UnityEngine;
using System.Collections;

//This class is for all objects the player can interact with
public abstract class InteractableObject : MonoBehaviour {
	protected abstract void InteractWith();

}
