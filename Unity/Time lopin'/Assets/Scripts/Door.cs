﻿using UnityEngine;
using System.Collections;

public class Door : AffectableObject {
	private bool opened;
	
	public override void Affect () {
		if(opened) {
			CloseDoor();
		}
		else {
			OpenDoor();	
		}
	}

	
	void OpenDoor() {
		gameObject.SetActive(false);
		opened = true;
		Debug.Log ("Door close");
	}
	
	void CloseDoor() {
		gameObject.SetActive(true);
		opened = false;
		Debug.Log ("Door open");
	}
	
}
