﻿using UnityEngine;
using System.Collections;

public class Shadow : MovingCharacter {
	public Recording myRecording;
	
	public override MovementState currentMovementState {
		get {
			return base.currentMovementState;
		}
		set {
			base.currentMovementState = value;
		}
	}
	
	protected override void FixedUpdate ()
	{
		base.FixedUpdate ();
		ReadRecording();
	}
	
	protected void ReadRecording () {
		if (myRecording.recordedMovementStates.Count > 0) {
			if (myRecording.ReadNextTimeStamp() <= Time.timeSinceLevelLoad) {
				currentMovementState = myRecording.ReadNextState();
				myRecording.RemoveNextState();
			}
		}
		else {
			PlaybackFinished();
		}
	}
	
	protected void PlaybackFinished() {
		enabled = false;
	}
	
	protected override void JustJumped ()
	{
		ReadRecording();
	}
}
