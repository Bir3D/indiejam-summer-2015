﻿using UnityEngine;
using System.Collections;

//This class is for all objects that can affect an AffectableObject Ex. a door
public abstract class LinkedToggleInteractable : LinkedInteractable {
	protected bool toggleOn;
	protected override void InteractWith () {
		if (toggleOn) {
			toggleOn = false;
			OnToggleOff();
		}
		else {
			toggleOn = true;
			OnToggleOn();
		}
	}
	
	protected virtual void OnToggleOn () {
		affectablePartner.Affect();
	}
	
	protected virtual void OnToggleOff () {
		affectablePartner.Affect();
	}
}
