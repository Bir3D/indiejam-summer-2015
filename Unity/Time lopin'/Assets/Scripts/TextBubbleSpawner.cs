﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBubbleSpawner : MonoBehaviour {

	public Text popupText;
	public Canvas popupCanvas;
	private int tutorialId;
	bool temp = true;

	void OnTriggerEnter(Collider col) {
		if (col.tag == "jumpTutorialCollider") {
			popupCanvas.enabled = true;
			popupText.text = "Press 'w' or 'up' to jump!";
			tutorialId = 1;
			Destroy(col.gameObject);
		}
		if (col.tag == "rewindTutorialCollider") {
			popupCanvas.enabled = true;
			popupText.text = "Press 'space' to rewind!";
			tutorialId = 2;
			Destroy(col.gameObject);
		}
		if (col.tag == "restartTutorialCollider") {
			popupCanvas.enabled = true;
			popupText.text = "Press 'backspace' to\n restart the level!";
		}
		if (col.tag == "interactTutorialCollider") {
			popupCanvas.enabled = true;
			popupText.text = "Press 'e' to interact!";
			tutorialId = 3;
			Destroy(col.gameObject);
		}
	}

	void Update() {
		if (tutorialId == 1) {
			if (Input.GetKeyDown(KeyCode.W)) {
				popupCanvas.enabled = false;
			}
		}
		if (tutorialId == 2) {
			if (Input.GetKeyDown(KeyCode.Space)) {
				popupCanvas.enabled = false;
			}
		}
		if (tutorialId == 3) {
			if (Input.GetKeyDown(KeyCode.E)) {
				popupCanvas.enabled = false;
			}
		}
		if (temp) {
			StartCoroutine(Delay());
		}
	}

	IEnumerator Delay() {
		yield return new WaitForSeconds (1);
		popupCanvas.enabled = false;
		temp = false;
	}
}