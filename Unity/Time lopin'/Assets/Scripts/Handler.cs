﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Handler : MonoBehaviour {
	public static Handler handlerInstance;
	private List<Recording> shadowRecordings = new List<Recording>();
	private Recording currentRecording;
	
	public IEnumerable<Recording> AllShadowRecordings () {
		foreach (Recording rec in shadowRecordings) {
			yield return rec;
		}
	}
	
	// Use this for initialization
	void Start () {
		if ( handlerInstance != null) {
			Destroy(gameObject);
		}
		else {
			DontDestroyOnLoad(gameObject);
			handlerInstance = this;
		}
	}
	
	public void StartNewShadowRecording () {
		currentRecording = new Recording();
		shadowRecordings.Add(currentRecording);
	}
	
	public void DeleteAllShadows () {
		shadowRecordings.Clear ();
	}
	
	public void AddState(MovementState stateToAdd) {
		currentRecording.AddState(stateToAdd, Time.timeSinceLevelLoad);
	}

}
